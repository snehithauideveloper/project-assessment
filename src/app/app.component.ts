import { Component } from '@angular/core';
import { AppService } from './service/app-.service';

export class User {
  name: string;
  phone: string;
  email: string;
  company: string;
  date_entry: string;
  org_num: string;
  address_1: string;
  city: string;
  zip: string;
  geo: string;
  pan: string;
  pin: string;
  id: number;
  status: string;
  fee: string;
  guid: string;
  date_exit: string;
  date_first: string;
  date_recent: string;
  url: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  responseMessage: string;

  usersDetails: User[];

  pageSize = 10;

  private allItems: User[];

  pager: any = {};

  pagedItems: any[];

  constructor(private service: AppService) {
    this.service.getJSON().subscribe(data => {
      this.allItems = data;
      this.setData(this.pageSize);
    }, err => {
      alert('A system error has occurred.');
    });
  }

  setData(pageSize) {
    this.usersDetails = JSON.parse(JSON.stringify(this.allItems));
    this.usersDetails = this.usersDetails.splice(0, pageSize);
    this.setPage(1);
  }

  pageSizeChange(pageSize) {
    this.setData(pageSize);
  }

  setPage(page: number) {
    this.usersDetails = JSON.parse(JSON.stringify(this.allItems));
    this.pager = this.service.getPager(this.usersDetails.length, page, this.pageSize);
    this.pagedItems = this.usersDetails.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

}
